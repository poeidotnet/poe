// Partie Actualité XB

var acto = document.querySelector('#panda');
acto.addEventListener("click",function(){document.querySelector('.actual').classList.add('visible');});

// var actc = document.querySelector('actu');
// actc.addEventListener("click",function(){document.querySelector('.actual').classList.remove('visible');});

var close = document.querySelector('croix')
close.addEventListener("click",function(){document.querySelector('.actual').classList.remove('visible');});

// Fin actualité
 

// Javascript de 'Mentions légales'


//gestion de l'ouverture et fermeture du modal
var blocmention = document.querySelector("blocmention")
var mloverlay = document.querySelector("mloverlay")

blocmention.addEventListener('click',function(){document.querySelector("mlmodal").classList.add("visible")})
mloverlay.addEventListener('click',function(){document.querySelector("mlmodal").classList.remove("visible")})

//navigation dans le modal
var mlb1 = document.querySelector("#mlbutton1")
var mlb2 = document.querySelector("#mlbutton2")
var mlb3 = document.querySelector("#mlbutton3")
var mlb4 = document.querySelector("#mlbutton4")

var mlpages = document.querySelector('mlpages');
var mlpagelist = mlpages.querySelectorAll('mlpages mlpage');
var mlcurrent = mlpagelist[0];

mlcurrent.classList.add('mlcurrent');

mlb1.addEventListener('click',function () {
    mlcurrent.classList.remove('mlcurrent');
    mlcurrent = mlpagelist[0];
    mlcurrent.classList.add('mlcurrent');
  })

mlb2.addEventListener('click',function () {
    mlcurrent.classList.remove('mlcurrent');
    mlcurrent = mlpagelist[1];
    mlcurrent.classList.add('mlcurrent');
  })

mlb3.addEventListener('click',function () {
    mlcurrent.classList.remove('mlcurrent');
    mlcurrent = mlpagelist[2];
    mlcurrent.classList.add('mlcurrent');
  })

mlb4.addEventListener('click',function () {
    mlcurrent.classList.remove('mlcurrent');
    mlcurrent = mlpagelist[3];
    mlcurrent.classList.add('mlcurrent');
  })


//gestion de la mise en page en cas de redimensionnement
var mlmodaldata = document.querySelector('mlmodaldata');

window.addEventListener('resize', function()
	{if (mlmodaldata.offsetWidth<535 && !(document.querySelectorAll("mlshortcut")[0].classList).contains('mlresized'))	
		{for(i=0;i<4;i++){document.querySelectorAll("mlshortcut")[i].classList.add('mlresized');}}
	else if (mlmodaldata.offsetWidth>535 && (document.querySelectorAll("mlshortcut")[0].classList).contains('mlresized'))
		{for(i=0;i<4;i++){document.querySelectorAll("mlshortcut")[i].classList.remove('mlresized');}}
	}
)

//Fin du javascript de 'Mentions légales'
